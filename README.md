# Privacy search engine list
A list of all the privacy search engines that are out there.

The rating system ranks from A to D

**A** Collects the least amount of data possible <br>
**B** Collects some data <br>
**C** Collects some data or has something strange in the privacy policy <br>
**D** Collects a lot of data avoid at all costs <br>

This list is in no particular order 

Name | Country | Privacy policy | Rating | CDN | Controversies | Collects Data | Meta 
-- | -- | -- | -- | -- | -- | -- |  -- |
DuckDuckGo | United States | https://duckduckgo.com/privacy | C | [Microsoft Azure](https://codeberg.org/swiso/website/issues/137#issuecomment-71831) | [Controversy](controversy) | Yes | Bing 
Blank | . |. |. |. |. |. |


# Open source search engine list